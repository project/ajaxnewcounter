<?php
/**
 * @file
 * Admin settings for AJAX new counter term.
 */

// Include for common form submit functions.
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'ajaxnewcounter') . '/ajaxnewcounter.admin.inc';

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function ajaxnewcounter_term_admin_settings($form, &$form_state) {

  //$form_state['cache'] = TRUE;
  $form['#tree'] = TRUE;

  // Get a list of all vocabularies.
  $vocabularies = taxonomy_get_vocabularies();

  $settings = variable_get('ajaxnewcounter_term_vocabs', array());
  $form['ajaxnewcounter_term_vocabs'] = array();
  $autopath = 'admin/config/user-interface/ajaxnewcounter/term/autocomplete/';

  // Render a fieldset for each vocabulary.
  foreach ($vocabularies as $vocabulary) {

    $vocab_name = $vocabulary->machine_name;
    // Merge in defaults.
    $settings += array(
      $vocab_name => array(
        'paths' => array(),
      )
    );
    $vocab = $settings[$vocab_name];

    // Add a field for every path plus one empty.
    if (empty($form_state['ajaxnewcounter_term_vocabs'][$vocab_name]['num_paths'])) {
      $num_paths = count($vocab['paths']);
      $form_state['ajaxnewcounter_term_vocabs'][$vocab_name]['num_paths'] = $num_paths + 1;
    }

    $form['ajaxnewcounter_term_vocabs'][$vocab_name] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($vocabulary->name),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $wrapper_id = drupal_html_id($vocab_name . '-paths');

    // Render the fields in a table.
    $form['ajaxnewcounter_term_vocabs'][$vocab_name]['paths'] = array(
      '#attributes' => array('id' => $wrapper_id),
      '#tree' => TRUE,
      '#theme' => 'table',
      '#header' => array(t('Taxonomy term'), t('Path'),
      ),
      '#rows' => array(),
    );

    // Build the number of path fieldsets indicated by $form_state['num_paths'].
    for ($i = 0; $i < $form_state['ajaxnewcounter_term_vocabs'][$vocab_name]['num_paths']; $i++) {

      // Default values for form elements.
      $default_term = '';
      $default_path = '';

      if (isset($vocab['paths'][$i])) {
        // We have saved values for this row.
        $default_path = $vocab['paths'][$i]['path'];
        if ($vocab['paths'][$i]['term'] != NULL) {
          // Convert saved tids to term names
          $saved_terms = taxonomy_term_load_multiple($vocab['paths'][$i]['term']);
          $term_names = array();
          foreach ($saved_terms as $saved_term) {
            $term_names[] = $saved_term->name;
          };
          $default_term = implode(',', $term_names);
        }
      }

      $term = array(
        '#id' => $vocab_name . '-term-' . $i,
        '#type' => 'textfield',
        '#description' => t("Enter a term name"),
        '#size' => 40,
        '#maxlength' => 255,
        '#autocomplete_path' => $autopath . $vocabulary->vid,
        '#type' => 'textfield',
        '#element_validate' => array('ajaxnewcounter_term_autocomplete_validate'),
        '#default_value' => $default_term,
      );

      $path = array(
        '#id' => $vocab_name . '-path-' . $i,
        '#type' => 'textfield',
        '#description' => t('The path that you want to connect to this term,
without slashes at the end or beginning. Leave empty to auto fill.'),
        '#size' => 60,
        '#maxlength' => 255,
        '#default_value' => $default_path,
      );

      $form['ajaxnewcounter_term_vocabs'][$vocab_name]['paths'][] = array(
        'term' => &$term,
        'path' => &$path,
      );
      $form['ajaxnewcounter_term_vocabs'][$vocab_name]['paths']['#rows'][] =  array(
        'data' => array(
          array('data' => &$term),
          array('data' => &$path),
        ),
        'class' => array($vocab_name . '-row-' . $i),
      );

      // Because we've used references we need to unset().
      unset($term);
      unset($path);
    }

    // Adds "Add another path" button.
    $form['ajaxnewcounter_term_vocabs'][$vocab_name]['add_path'] = array(
      '#type' => 'submit',
      '#name' => $vocab_name . '-add',
      '#value' => t('Add more paths'),
      '#submit' => array('ajaxnewcounter_term_admin_settings_add_path'),
    );
    // Select all terms.
    $form['ajaxnewcounter_term_vocabs'][$vocab_name]['all'] = array(
      '#type' => 'checkbox',
      '#title' => t('All terms from this vocabulary'),
      '#default_value' => isset($vocab['all']) ? $vocab['all'] : '',
    );
  }

  $form = system_settings_form($form);

  // Reorder the submit functions, place ours first.
  $form['#submit'][] = $form['#submit'][0];
  $form['#submit'][] = 'ajaxnewcounter_form_submit';
  $form['#submit'][0] = 'ajaxnewcounter_term_form_submit';

  return $form;
}


/**
 * Submit handler for "Add more paths" button on ajaxnewcounter_term_admin_settings().
 *
 * $form_state['num_paths'] tells the form builder function how many path
 * rows to build, so here we increment it.
 */
function ajaxnewcounter_term_admin_settings_add_path($form, &$form_state) {
  $button = $form_state['triggering_element'];
  $form_state['ajaxnewcounter_term_vocabs'][$button['#parents'][1]]['num_paths'] += 5;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit function for ajaxnewcounter_term_admin_settings().
 */
function ajaxnewcounter_term_form_submit($form, &$form_state) {
  foreach ($form_state['values']['ajaxnewcounter_term_vocabs'] as $vocab => $paths) {
    $count = count($paths['paths']);
    for ($i = 0; $i < $count; $i++) {
      if ($paths['paths'][$i]['term'] == NULL) {
        unset($paths['paths'][$i]);
      }
    }
    $form_state['values']['ajaxnewcounter_term_vocabs'][$vocab]['paths'] = array_values($paths['paths']);
  }
}

/**
 * Page callback: Outputs JSON for taxonomy autocomplete suggestions.
 *
 * Path: admin/config/user-interface/ajaxnewcounter/term/autocomplete
 *
 * Copied from taxonomy_autocomplete and modified to take a vid instead of
 * a field as first argument.
 *
 * @param $vid
 *   Vocabulary id.
 * @param $tags_typed
 *   (optional) A comma-separated list of term names entered in the
 *   autocomplete form element. Only the last term is used for autocompletion.
 *   Defaults to '' (an empty string).
 *
 * @see ajaxnewcounter_term_menu()
 * @see taxonomy_autocomplete()
 */
function ajaxnewcounter_term_autocomplete($vid, $tags_typed = '') {
  // If the request has a '/' in the search text, then the menu system will have
  // split it into multiple arguments, recover the intended $tags_typed.
  $args = func_get_args();
  // Shift off the $vid argument.
  array_shift($args);
  $tags_typed = implode('/', $args);

  // Make sure the vid exists.
  if (!taxonomy_vocabulary_load($vid)) {
    // Error string. The JavaScript handler will realize this is not JSON and
    // will display it as debugging information.
    print t('Taxonomy id @vid not found.', array('@vid' => $vid));
    exit;
  }

  // The user enters a comma-separated list of tags. We only autocomplete the last tag.
  $tags_typed = drupal_explode_tags($tags_typed);
  $tag_last = drupal_strtolower(array_pop($tags_typed));

  if ($tag_last != '') {

    $vids = array($vid);

    $query = db_select('taxonomy_term_data', 't');
    $query->addTag('translatable');
    $query->addTag('term_access');

    // Do not select already entered terms.
    if (!empty($tags_typed)) {
      $query->condition('t.name', $tags_typed, 'NOT IN');
    }
    // Select rows that match by term name.
    $tags_return = $query
      ->fields('t', array('tid', 'name'))
      ->condition('t.vid', $vids)
      ->condition('t.name', '%' . db_like($tag_last) . '%', 'LIKE')
      ->range(0, 10)
      ->execute()
      ->fetchAllKeyed();

    $prefix = count($tags_typed) ? drupal_implode_tags($tags_typed) . ', ' : '';

    $term_matches = array();
    foreach ($tags_return as $tid => $name) {
      $n = $name;
      // Term names containing commas or quotes must be wrapped in quotes.
      if (strpos($name, ',') !== FALSE || strpos($name, '"') !== FALSE) {
        $n = '"' . str_replace('"', '""', $name) . '"';
      }
      $term_matches[$prefix . $n] = check_plain($name);
    }
  }

  drupal_json_output($term_matches);
}


/**
 * Form element validate handler for taxonomy term autocomplete element.
 */
function ajaxnewcounter_term_autocomplete_validate($element, &$form_state) {
  // Autocomplete widgets do not send their tids in the form, so we must detect
  // them here and process them independently.
  $value = array();
  if ($tags = $element['#value']) {
    // Collect candidate vocabularies.
    $vocabularies = array();
    if ($vocabulary = taxonomy_vocabulary_machine_name_load($element['#parents'][1])) {
      $vocabularies[] = $vocabulary;
    }

    // Translate term names into actual terms.
    $typed_terms = drupal_explode_tags($tags);
    foreach ($typed_terms as $typed_term) {
      // See if the term exists in the chosen vocabulary and return the tid;
      if ($possibilities = taxonomy_term_load_multiple(array(), array('name' => trim($typed_term), 'vid' => $vocabularies[0]->vid))) {
        $term = array_pop($possibilities);
        $value[] = $term->tid;
      }
      else {
        $element_name = implode('][', $element['#parents']);
        form_set_error($element_name, t('@term is not a valid taxonomy term.', array('@term' => check_plain($element['#value']))));
      }
    }
  }

  if ($value) form_set_value($element, $value, $form_state);
}
