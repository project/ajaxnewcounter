
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Author / Maintainer: Andreas Radloff
Project Page:

This module adds iOS-style badges to links displaying how many new nodes
(or comments or users) has been added to the page since the last time
the user visited that page.

You can for example add count badges to your taxonomy links, menu items
or a link to a View of the latest comments or users on your site.
Badges are only added to links that the user has previously visited.

The badges are added client-side through javascript and works well with
anonymous users as well as with caching and the Boost module.

This is done by storing a page visit history in the browsers javascript
Local Storage, and by generating a json-file server side that contains
a list of timestamps for each path that the administrator har enabled
counting for. It should have very low impact on performance.

Local storage is supported by IE 8, Chrome 4, FIrefox 3.5 and Safari.
See http:// dev-test.nemikor.com/web-storage/support-test/.

The module comes with two submodules, one for taxonomy terms and one for
views. A hook is provided for easy extension of supported page types.
The submodules are responsible for collecting the list of timestamps for
a given url. The views submodule can be used to connect any view to any
url, meaning you can use it for listing new users, comments or nodes and
attach that count to any link.

INSTALLATION
------------

* Download json2.js from https:// github.com/douglascrockford/JSON-js and
  place it in sites/all/libraries/json2/json2.js
* Enable the module and at least one submodule
* Visit the configuration page and add one or more paths to watch for news
