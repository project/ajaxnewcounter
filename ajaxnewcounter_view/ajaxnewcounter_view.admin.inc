<?php
/**
 * @file
 * Admin settings for AJAX new counter view.
 */

// Include for common form submit functions.
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'ajaxnewcounter') . '/ajaxnewcounter.admin.inc';

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function ajaxnewcounter_view_admin_settings($form, &$form_state) {

  //$form_state['cache'] = TRUE;
  $form['#tree'] = TRUE;
  $form['ajaxnewcounter_view_views'] = array(
    '#tree' => TRUE,
  );

  $settings = variable_get('ajaxnewcounter_view_views', array());

  // Merge in defaults.
  $settings += array(
    'paths' => array(),
  );

  // Get a list of all views.
  $views = views_get_all_views();

  $list = array();
  foreach ($views as $name => $view) {
    foreach ($view->display as $id => $display) {
      $key = $name . '/' . $id;
      $has_path = isset($display->display_options['path']) ? '*' : '';
      $list[$key] = $view->human_name . ' > ' . $display->display_title . $has_path;
    }
  }

  // Add a field for every path plus one empty.
  if (empty($form_state['num_paths'])) {
    $num_paths = count($settings['paths']);
    $form_state['num_paths'] = $num_paths + 1;
  }

  // Render the fields in a table.
  $form['ajaxnewcounter_view_views']['paths'] = array(
    '#tree' => TRUE,
    '#theme' => 'table',
    '#header' => array(t('Path'), t('view') . ' > ' . t('Display'), t('Arguments')),
    '#rows' => array(),
  );

  // Add some help text.
  $form['ajaxnewcounter_view_views']['paths']['#rows'][] =  array(
    'data' => array(
      array('data' => t('The path that you want to add counters to.')),
      array('data' => t('* = Path can be auto-detected')),
      array('data' => t('Arguments to pass to the view separated by a slash, like arg1/arg2/etc.')),
    ),
  );

  // Build the number of path fieldsets indicated by $form_state['num_paths'].
  for ($i = 0; $i < $form_state['num_paths']; $i++) {

    // Default values for form elements.
    $default_path = '';
    $default_view = '';
    $default_args = '';

    if (isset($settings['paths'][$i])) {
      // We have saved values for this row.
      $setting = $settings['paths'][$i];
      $default_path = $setting['path'];
      $default_view = $setting['view'];
      $default_args = $setting['args'];
    }

    $path_hint = '';

    // See if we can auto-detect the path of the selected view.
    if ($default_view != '' && $default_path == '') {
      $autodetect = ajaxnewcounter_view_autodetect($setting);
      if ($autodetect) {
        // This is a view that has a path.
        $autopath = array_keys($autodetect);
        $path_hint = t('Autodetected as') . ' "' . check_plain($autopath[0]) . '".';
      }
    }

    $path = array(
      '#id' => 'path-' . $i,
      '#type' => 'textfield',
      '#description' => $path_hint,
      '#size' => 60,
      '#maxlength' => 255,
      '#default_value' => $default_path,
    );

    $select_view = array(
      '#id' => 'view-' . $i,
      '#type' => 'select',
      '#options' => $list,
      '#default_value' => $default_view,
      '#empty_option' => t('Auto-detect view and arguments from path'),
    );

    $arguments = array(
      '#id' => 'args-' . $i,
      '#type' => 'textfield',
      '#size' => 60,
      '#maxlength' => 255,
      '#default_value' => $default_args,
      '#states' => array(
        'invisible' => array(
          ':input[id="view-' . $i . '"]' => array('value' => ''),
        ),
      ),
    );

    $form['ajaxnewcounter_view_views']['paths'][] = array(
      'path' => &$path,
      'view' => &$select_view,
      'args' => &$arguments,
    );
    $form['ajaxnewcounter_view_views']['paths']['#rows'][] =  array(
      'data' => array(
        array('data' => &$path),
        array('data' => &$select_view),
        array('data' => &$arguments),
      ),
      'class' => array('row-' . $i),
    );

    // Because we've used references we need to unset().
    unset($arguments);
    unset($select_view);
    unset($path);
  }

  // Adds "Add more paths" button.
  $form['ajaxnewcounter_view_views']['add_path'] = array(
    '#type' => 'submit',
    '#value' => t('Add more paths'),
    '#submit' => array('ajaxnewcounter_view_admin_settings_add_path'),
  );

  $form = system_settings_form($form);

  // Reorder the submit functions, place ours first.
  $form['#submit'][] = $form['#submit'][0];
  //$form['#submit'][] = 'ajaxnewcounter_form_submit';
  $form['#submit'][0] = 'ajaxnewcounter_view_form_submit';

  return $form;
}


/**
 * Submit handler for "Add more paths" button on ajaxnewcounter_view_admin_settings().
 *
 * $form_state['num_paths'] tells the form builder function how many path
 * rows to build, so here we increment it.
 */
function ajaxnewcounter_view_admin_settings_add_path($form, &$form_state) {
  $form_state['num_paths'] += 5;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit function for ajaxnewcounter_term_admin_settings().
 */
function ajaxnewcounter_view_form_submit($form, &$form_state) {
  $paths = $form_state['values']['ajaxnewcounter_view_views']['paths'];
  $count = count($paths);
  for ($i = 0; $i < $count; $i++) {
    if ($paths[$i]['path'] == '' && $paths[$i]['view'] == '') {
      unset($paths[$i]);
    }
  }
  $form_state['values']['ajaxnewcounter_view_views']['paths'] = array_values($paths);
}

/**
 * Form validation.
 */
function ajaxnewcounter_view_admin_settings_validate($form, &$form_state) {
  foreach ($form_state['values']['ajaxnewcounter_view_views']['paths'] as $delta => $value) {
    $path = $value['path'];
    if ($path) {
      $element_name = "ajaxnewcounter_view_views][paths][$delta][path";
      if (!ajaxnewcounter_view_lookup_view($path) && $value['view'] == '') {
        form_set_error($element_name, t('"@path" is not a valid views path.', array('@path' => check_plain($path))));
      }
      elseif ($value['view'] != '' && !drupal_valid_path($path)) {
        form_set_error($element_name, t('"@path" is not a valid path.', array('@path' => check_plain($path))));
      }
    }
    else {
      $return = ajaxnewcounter_view_autodetect($value);
      if (!$return) {
        // We have a view but no path and the view has no path.
        $element_name = "ajaxnewcounter_view_views][paths][$delta][view";
        form_set_error($element_name, t('View @view has no path we can auto-detect. Please provide any valid path.', array('@view' => check_plain($value['view']))));
      }
    }
  }
}
