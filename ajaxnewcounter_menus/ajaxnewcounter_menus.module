<?php
/**
 * @file
 * Fetches a list of taxonomy terms
 */

/**
 * Implements hook_menu().
 */
function ajaxnewcounter_menus_menu() {
  $items['admin/config/user-interface/ajaxnewcounter/menus'] = array(
    'title' => 'AJAX new counter menus',
    'description' => 'Perform maintenance tasks for AJAX new counter menus.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('ajaxnewcounter_menus_admin_settings'),
    'access arguments' => array('administer ajaxnewcounter'),
    'file' => 'ajaxnewcounter_menus.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
* Given a path, return a term.
*
* @param string $path
*   A path.
*
* @return bool|array
*   FALSE or a term object
*/
function ajaxnewcounter_menus_get_term($path) {
  $alias = drupal_lookup_path('source', $path);
  $path = $alias ? $alias : $path;
  // This seems to be the easiest way to check if it's a term even if the path
  // is overridden by Views.
  $router_item = menu_get_item($path);
  if ($router_item['path'] == 'taxonomy/term/%') {
    $last = array_pop($router_item['map']);
    return is_object($last) ? $last : taxonomy_term_load($last);
  }
  else {
    return FALSE;
  }
}

/**
 * Implements hook_ajaxnewcounter_paths().
 *
 * @param int $limit
 *   The maximum number of entries to return.
 *
 * @return array
 *   Array.
 */
function ajaxnewcounter_menus_ajaxnewcounter_paths($limit) {
  $output = array();
  $menus = variable_get('ajaxnewcounter_menus_menus', array());
  // Check which sub-modules are enabled.
  $anc_terms = function_exists('ajaxnewcounter_term_get_nodes');
  $anc_views = function_exists('ajaxnewcounter_view_get_items');
  foreach ($menus as $machine_name => $enabled) {
    if ($enabled) {
      // Load menu items.
      $items = menu_load_links($machine_name);
      // Loop through menu items and see if they can be processed.
      foreach ($items as $item) {
        $path = drupal_get_path_alias($item['link_path']);
        // Check if this is a taxonomy term page.
        if($anc_terms && $term = ajaxnewcounter_menus_get_term($path)) {
          $output[$path] = ajaxnewcounter_term_get_nodes($term->tid, $limit);
        }
        // Check if this is a view.
        elseif ($anc_views && $result = ajaxnewcounter_view_load_view($path)) {
          $output[$path] = ajaxnewcounter_view_get_items($result, $limit);
        }
      }
    }
  }
  return $output;
}

/**
 * Implements hook_menu_link_insert().
 */
function ajaxnewcounter_menus_menu_link_insert($link) {
  ajaxnewcounter_menus_on_menu_link_save($link);
}

/**
 * Implements hook_menu_link_update().
 */
function ajaxnewcounter_menus_menu_link_update($link) {
  ajaxnewcounter_menus_on_menu_link_save($link);
}

/**
 * Implements hook_menu_link_delete().
 */
function ajaxnewcounter_menus_menu_link_delete($link) {
  ajaxnewcounter_menus_on_menu_link_save($link);
}

/**
 * Update the file.
 */
function ajaxnewcounter_menus_on_menu_link_save($link) {
  $menus = variable_get('ajaxnewcounter_menus_menus', array());
  // Check if the menu is selected.
  if (isset($menus[$link['menu_name']]) && $menus[$link['menu_name']]) {
    ajaxnewcounter_list_make();
  }
}
