<?php
/**
 * @file
 * Admin settings for AJAX new counter menus.
 */

// Include for common form submit functions.
include_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'ajaxnewcounter') . '/ajaxnewcounter.admin.inc';

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function ajaxnewcounter_menus_admin_settings() {
  $settings = variable_get('ajaxnewcounter_menus_menus', array());
  // Fetch all menus
  $menus = menu_get_menus();

  $form['ajaxnewcounter_menus_menus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add counters to the following menus'),
    '#collapsible' => FALSE,
    "#tree" => TRUE,
  );

  foreach ($menus as $machine_name => $menu) {
    $form['ajaxnewcounter_menus_menus'][$machine_name] = array(
      '#type' => 'checkbox',
      '#title' => $menu,
      '#default_value' => isset($settings[$machine_name]) ? $settings[$machine_name] : 0,
    );
  }

  $form = system_settings_form($form);

  $form['#submit'][] = 'ajaxnewcounter_form_submit';

  return $form;

}