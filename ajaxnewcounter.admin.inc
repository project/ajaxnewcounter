<?php
/**
 * @file
 * Admin settings for AJAX new counter
 */

/**
 * Administration settings form.
 *
 * @see system_settings_form()
 */
function ajaxnewcounter_admin_settings() {
  $form['ajaxnewcounter_json_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to json2.js'),
    '#default_value' => variable_get('ajaxnewcounter_json_path', 'sites/all/libraries/json2/json2.js'),
    '#maxlength' => 255,
    '#description' => t("Get it from https:// github.com/douglascrockford/JSON-js"),
    '#required' => TRUE,
  );
  $form['ajaxnewcounter_jqpre'] = array(
    '#type' => 'textfield',
    '#title' => t('jQuery selector prefix'),
    '#default_value' => variable_get('ajaxnewcounter_jqpre', ''),
    '#maxlength' => 255,
    '#description' => t('How links will be identified. Pattern: <em>Prefix</em> a[href=path]<em>Suffix</em>. For example <em>#header</em>.'),
  );
  $form['ajaxnewcounter_jqpost'] = array(
    '#type' => 'textfield',
    '#title' => t('jQuery selector suffix'),
    '#default_value' => variable_get('ajaxnewcounter_jqpost', ':first'),
    '#maxlength' => 255,
    '#description' => t('How links will be identified. Pattern: <em>Prefix</em> a[href=path]<em>Suffix</em>. Defaults to <em>:first</em> for the first occurande of the link.'),
  );

  $form['ajaxnewcounter_maxnodes'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of nodes to count per link'),
    '#default_value' => variable_get('ajaxnewcounter_maxnodes', 20),
    '#size' => 4,
    '#maxlength' => 4,
    '#description' => t("If you have lots of paths this number increases the json file size."),
    '#required' => TRUE,
  );
  $form['ajaxnewcounter_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Accuracy and interval'),
    '#default_value' => variable_get('ajaxnewcounter_interval', 60),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("How accurate should the timestamps be, in seconds.
Also effects how often the user will retrieve a fresh version of the file from the server.
Default is 60, which means the user will use the browsers cached file
if it is not older than 60 seconds. High value decreases file size."),
    '#required' => TRUE,
  );
  $form['ajaxnewcounter_generate_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate node list on cron'),
    '#default_value' => variable_get('ajaxnewcounter_generate_cron', TRUE),
  );
  $form['ajaxnewcounter_generate_node_save'] = array(
    '#type' => 'checkbox',
    '#title' => t('Generate node list on node save, update and delete'),
    '#default_value' => variable_get('ajaxnewcounter_generate_node_save', TRUE),
  );
  $form['ajaxnewcounter_delay_on_node_save'] = array(
    '#type' => 'checkbox',
    '#title' => t('Delay generation on node save until next page load, prevents multiple generations on node batch operations.'),
    '#default_value' => variable_get('ajaxnewcounter_delay_on_node_save', TRUE),
  );
  $form = system_settings_form($form);

  $form['#submit'][] = 'ajaxnewcounter_form_submit';

  return $form;

}

/**
 * Validate settings form.
 */
function ajaxnewcounter_admin_settings_validate($form, &$form_state) {
  if (!file_exists(DRUPAL_ROOT . '/' . $form_state['values']['ajaxnewcounter_json_path'])) {
    form_set_error('ajaxnewcounter_json_path', t('File not found in this path.'));
  }
  $max = $form_state['values']['ajaxnewcounter_maxnodes'];
  if (!is_numeric($max)) {
    form_set_error('ajaxnewcounter_maxnodes', t('You must enter an integer for the maximum number of nodes.'));
  }
  elseif ($max <= 0) {
    form_set_error('ajaxnewcounter_maxnodes', t('Maximum number of nodes must be positive.'));
  }
  $interval = $form_state['values']['ajaxnewcounter_interval'];
  if (!is_numeric($interval)) {
    form_set_error('ajaxnewcounter_interval', t('You must enter an integer for the accuracy and interval.'));
  }
  elseif ($interval <= 0) {
    form_set_error('ajaxnewcounter_interval', t('accuracy and interval must be positive.'));
  }
}

/**
 * Submit handler for admin form.
 */
function ajaxnewcounter_form_submit($form, &$form_state) {
  ajaxnewcounter_list_make();
}
