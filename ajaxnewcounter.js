(function($) {
  Drupal.behaviors.ajaxnewcounter = {
    attach: function (/* context, settings */) {

      if (typeof(Storage) !== "undefined" && !$('body').hasClass('anc-processed')) {
        $('body').addClass('anc-processed');
        // Prevent caching and only refresh the browser cache at set interval
        var timetoken = Math.round((new Date()).getTime() / 1000 / parseInt(Drupal.settings.ajaxnewcounter.interval));
        var jsonurl = Drupal.settings.ajaxnewcounter.datapath + '?now=' + timetoken;
        $.getJSON(jsonurl, function(data) {
          var jsonpaths = localStorage.ajaxnewcounter;
          var paths = Drupal.settings.ajaxnewcounter.paths = (typeof jsonpaths === 'string') ? JSON.parse(jsonpaths) : {};

          $.each(data, function(key, val) {
            ajaxnewcounter_recursive(key, val);
          });

          function ajaxnewcounter_recursive(key, val, path, tree, branch) {
            path = typeof path !== 'undefined' ? path + '/' + key : key;
            if (typeof tree === 'undefined') {
              tree = paths;
            }
            if (typeof branch === 'undefined') {
              branch = tree;
            }
            branch[key] = typeof branch[key] !== 'undefined' ? branch[key] : {};
            branch = branch[key];

            for (var k in val) {
              if (k == '_') {
                ajaxnewcounter_evaluate(key, val['_'], tree, branch, path)
              } else {
                ajaxnewcounter_recursive(k, val[k], path, tree, branch);
              }
            }
          }

          function ajaxnewcounter_evaluate(key, val, tree, branch, path) {
            if ('/' + path == window.location.pathname) {
              branch['_'] = Math.round((new Date()).getTime() / 1000);
            } else {
              if (typeof branch['_'] != 'undefined') {
                var lastvisit = Math.round(parseInt(branch['_']) / parseInt(Drupal.settings.ajaxnewcounter.interval));
                var count = 0;
                // Decode base 36
                var latest = parseInt(val[0], 36);
                while (latest >= lastvisit && (typeof val[count] != 'undefined')) {
                  count++;
                  latest -= parseInt(val[count], 36);
                }
                if (count > 0) {
                  var jqpre = Drupal.settings.ajaxnewcounter.jqpre;
                  var jqpost = Drupal.settings.ajaxnewcounter.jqpost;
                  var identifier = (jqpre != '') ? jqpre + ' ' : '';
                  identifier += 'a[href ="/' + path + '"]';
                  identifier += typeof jqpost != '' ? jqpost : '';
                  var link = $(identifier);
                  link.addClass('link-badge-badge-wrapper').append('<span class="link-badge">' + count + '</span>');
                }
              }
            }
          }
          localStorage.ajaxnewcounter = JSON.stringify(paths);
        });
      }
    }
  };
})(jQuery);
